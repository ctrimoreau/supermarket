# supermarket


## Execute

```
	JUnit: mvn clean test
	Package: mvn clean package
	Run: java -jar artefactname.jar
	
	
```



## Framework and other

Only JUnit will be used. Although Spring Boot could have been used, lets keep things simple.

TDD will be used to test and expect user story.

Using as much as BigDecimal as possible to prevent computation errors.



## Unit test

Coverage is 100% for model and service using TDD



## Supposition

Subject wasn't clear. Some supposition have been taken.

Since no details on the basket have been asked, only the final price will be shown.

#### Apple

If you buy one apple, you get one apple and you pay 0.2 (1 x 0.2).

If you buy two apple, you get two apple and you pay 0.2 (1 x 0.2 + 1 free).

If you buy three apple, you get three apple and you pay 0.4 (2 x 0.2 + 1 free).

If you buy four apple, you get four apple and you pay 0.4 (2 x 0.2 + 2 free).

...

#### Orange

No discount

#### Watermelon

If you buy one watermelon, you get one watermelon and you pay 0.8 (1 x 0.8).

If you buy two watermelon, you get two watermelon and you pay 1.6 (2 x 0.8).

If you buy three watermelon, you get three watermelon and you pay 1.6 (2 x 0.8 + 1 free).

If you buy four watermelon, you get four watermelon and you pay 2.4 (3 x 0.8 + 1 free).

...