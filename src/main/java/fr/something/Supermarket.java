package fr.something;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import fr.something.model.Apple;
import fr.something.model.Fruits;
import fr.something.model.Orange;
import fr.something.model.Watermelon;
import fr.something.service.DiscountService;
import fr.something.service.DiscountServiceImpl;

public class Supermarket {
	
	public static void main(String[] args) {

		System.out.println("******* Supermarket Main as exemple *******");
		
		// Data
		Fruits apple1 = new Apple();
		Fruits apple2 = new Apple();
		Fruits apple3 = new Apple();
		Fruits apple4 = new Apple();
		
		Fruits orange1 = new Orange();
		Fruits orange2 = new Orange();
		Fruits orange3 = new Orange();
		
		Fruits watermelon1 = new Watermelon();
		Fruits watermelon2 = new Watermelon();
		Fruits watermelon3 = new Watermelon();
		Fruits watermelon4 = new Watermelon();
		Fruits watermelon5 = new Watermelon();
		
		List<Fruits> fruits = new ArrayList<>();
		fruits.add(apple1);
		fruits.add(apple2);
		fruits.add(apple3);
		fruits.add(apple4);
		
		fruits.add(orange1);
		fruits.add(orange2);
		fruits.add(orange3);
		
		fruits.add(watermelon1);
		fruits.add(watermelon2);
		fruits.add(watermelon3);
		fruits.add(watermelon4);
		fruits.add(watermelon5);
		
		// Execute
		DiscountService discountService = new DiscountServiceImpl();
		Float resultTotalPrice = discountService.computeDiscount(fruits);
		
		DecimalFormat format = new DecimalFormat("0.00");
		
		System.out.println("Total price for 4 apple, 3 orange and 5 watermelon is '" + format.format(resultTotalPrice) + "'");
		
	}

}
