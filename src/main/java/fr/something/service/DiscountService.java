package fr.something.service;

import java.util.List;

import fr.something.model.Fruits;

public interface DiscountService {

	/**
	 * Compute the final price of a list of fruits by taking the discount into the result.
	 * @param fruits the list of fruits on which we when to compute the price including the discount.
	 * @return price including the discount.
	 */
	Float computeDiscount(List<Fruits> fruits);
	
}
