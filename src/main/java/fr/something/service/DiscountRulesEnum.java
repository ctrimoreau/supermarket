package fr.something.service;

public enum DiscountRulesEnum {

	Apple(2), Watermelon(3), Orange(null);
	
	private Integer range;
	
	private DiscountRulesEnum(Integer theRange) {
		range = theRange;
	}

	public Integer getRange() {
		return range;
	}
	
}
