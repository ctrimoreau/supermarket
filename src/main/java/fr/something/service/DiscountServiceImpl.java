package fr.something.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import fr.something.model.Fruits;

public class DiscountServiceImpl implements DiscountService {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Float computeDiscount(List<Fruits> fruits) {
		
		// Count the number of fruits per fruits category
		Map<String, Long> nbFruitsPerCategories = extractFruitsQuantity(fruits);
		
		// Get the price of each fruits
		Map<String, Float> fruitsPricePerCategories = extractFruitsPrices(fruits);

		
		// Total price
		BigDecimal price = new BigDecimal(0.0);
		
		for (Entry<String, Long> currentFruitsCategory : nbFruitsPerCategories.entrySet()) {

			// Fruits category
			String fruitsCategory = currentFruitsCategory.getKey();
			
			// Fruits price
			BigDecimal fruitsPrice = new BigDecimal(fruitsPricePerCategories.get(fruitsCategory)).setScale(2, RoundingMode.HALF_UP);
			
			// Number of fruits
			int nbFruits = currentFruitsCategory.getValue().intValue();
			
			// By default we set the number of fruits without discount to the total number of fruits
			// If we find a discount for this category of fruit we then compute the real number of fruits without a discount
			BigDecimal fruitsExcludingDiscount = new BigDecimal(currentFruitsCategory.getValue().intValue());
			DiscountRulesEnum discountRules = DiscountRulesEnum.valueOf(fruitsCategory);
			if (discountRules.getRange() != null) {
				fruitsExcludingDiscount = new BigDecimal(nbFruits - (nbFruits / discountRules.getRange()));
			}
			
			// Current fruits price
			BigDecimal currentFruitsPrice = fruitsPrice.multiply(fruitsExcludingDiscount).setScale(2, RoundingMode.HALF_UP);
			
			// Compute price
			price = price.add(currentFruitsPrice);
		}
		
		return price.floatValue();
	}
	
	/**
	 * Extract the number of fruits for each kind of fruits
	 * @param fruits the list of fruits.
	 * @return number of fruits per category of fruits
	 */
	private Map<String, Long> extractFruitsQuantity(List<Fruits> fruits) {
		return fruits.stream().collect(
				Collectors.groupingBy(Fruits::name, Collectors.counting()
                ));
	}
	
	/**
	 * Extract the price of each fruits.
	 * @param fruits the list of fruits.
	 * @return price of each fruits.
	 */
	private Map<String, Float> extractFruitsPrices(List<Fruits> fruits) {
		Map<String, Float> result = new HashMap<>();
		for (Fruits current : fruits) {
			if (!result.containsKey(current.name())) {
				result.put(current.name(), current.price());
			}
		}
		return result;
	}

}
