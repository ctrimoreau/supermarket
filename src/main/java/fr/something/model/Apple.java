package fr.something.model;

public class Apple implements Fruits {

	@Override
	public Float price() {
		return 0.2f;
	}

	@Override
	public String name() {
		return "Apple";
	}
	
}
