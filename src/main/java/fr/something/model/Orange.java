package fr.something.model;

public class Orange implements Fruits {

	@Override
	public Float price() {
		return 0.5f;
	}
	
	@Override
	public String name() {
		return "Orange";
	}
	
}
