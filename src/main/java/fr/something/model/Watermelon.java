package fr.something.model;

public class Watermelon implements Fruits {

	@Override
	public Float price() {
		return 0.8f;
	}

	@Override
	public String name() {
		return "Watermelon";
	}
	
}
