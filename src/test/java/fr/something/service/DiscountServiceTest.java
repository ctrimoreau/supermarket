package fr.something.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import fr.something.model.Apple;
import fr.something.model.Fruits;
import fr.something.model.Orange;
import fr.something.model.Watermelon;

public class DiscountServiceTest {

	private DiscountService discountService;
	
    @Before
    public void setUp() {
    	discountService = new DiscountServiceImpl();
    }
	
	@Test
	public void testComputeDiscount_case1() {
		
		// Data
		Fruits apple1 = new Apple();
		Fruits orange1 = new Orange();
		Fruits watermelon1 = new Watermelon();
		
		List<Fruits> fruits = new ArrayList<>();
		fruits.add(apple1);
		fruits.add(orange1);
		fruits.add(watermelon1);
		
		// Expected
		// 1 apple (0.2) + 1 orange (0.5) + 1 watermelon (0.8) = 0.2 + 0.5 + 0.8 = 1.5
		Float expectedTotalPrice = 1.5f;
		
		// Execute
		Float resultTotalPrice = discountService.computeDiscount(fruits);
		
		// Assert
		Assert.assertEquals(expectedTotalPrice, resultTotalPrice, 0.0f);
		
	}
	
	@Test
	public void testComputeDiscount_case2() {
		
		// Data
		Fruits apple1 = new Apple();
		Fruits apple2 = new Apple();
		Fruits apple3 = new Apple();
		Fruits apple4 = new Apple();
		Fruits apple5 = new Apple();
		
		List<Fruits> fruits = new ArrayList<>();
		fruits.add(apple1);
		fruits.add(apple2);
		fruits.add(apple3);
		fruits.add(apple4);
		fruits.add(apple5);
		
		// Expected
		// 5 apple (3x 0.2 + 2 free)
		Float expectedTotalPrice = 0.6f;
		
		// Execute
		Float resultTotalPrice = discountService.computeDiscount(fruits);
		
		// Assert
		Assert.assertEquals(expectedTotalPrice, resultTotalPrice, 0.0f);
		
	}
	
	
	@Test
	public void testComputeDiscount_case3() {
		
		// Data
		Fruits orange1 = new Orange();
		Fruits orange2 = new Orange();
		Fruits orange3 = new Orange();
		Fruits orange4 = new Orange();
		
		List<Fruits> fruits = new ArrayList<>();
		fruits.add(orange1);
		fruits.add(orange2);
		fruits.add(orange3);
		fruits.add(orange4);
		
		// Expected
		// 4 orange (4x 0.5)
		Float expectedTotalPrice = 2.0f;
		
		// Execute
		Float resultTotalPrice = discountService.computeDiscount(fruits);
		
		// Assert
		Assert.assertEquals(expectedTotalPrice, resultTotalPrice, 0.0f);
		
	}
	
	@Test
	public void testComputeDiscount_case4() {
		
		// Data		
		Fruits watermelon1 = new Watermelon();
		Fruits watermelon2 = new Watermelon();
		Fruits watermelon3 = new Watermelon();
		Fruits watermelon4 = new Watermelon();
		Fruits watermelon5 = new Watermelon();
		Fruits watermelon6 = new Watermelon();
		Fruits watermelon7 = new Watermelon();
		Fruits watermelon8 = new Watermelon();
		
		List<Fruits> fruits = new ArrayList<>();
		fruits.add(watermelon1);
		fruits.add(watermelon2);
		fruits.add(watermelon3);
		fruits.add(watermelon4);
		fruits.add(watermelon5);
		fruits.add(watermelon6);
		fruits.add(watermelon7);
		fruits.add(watermelon8);
		
		// Expected
		// 8 watermelon (6x 0.8 + 2 free)
		Float expectedTotalPrice = 4.8f;
		
		// Execute
		Float resultTotalPrice = discountService.computeDiscount(fruits);
		
		// Assert
		Assert.assertEquals(expectedTotalPrice, resultTotalPrice, 0.0f);
		
	}
	
	@Test
	public void testComputeDiscount_case5() {
		
		// Data
		Fruits apple1 = new Apple();
		Fruits apple2 = new Apple();
		Fruits apple3 = new Apple();
		Fruits apple4 = new Apple();
		Fruits apple5 = new Apple();
		
		Fruits orange1 = new Orange();
		Fruits orange2 = new Orange();
		Fruits orange3 = new Orange();
		Fruits orange4 = new Orange();
		
		Fruits watermelon1 = new Watermelon();
		Fruits watermelon2 = new Watermelon();
		Fruits watermelon3 = new Watermelon();
		Fruits watermelon4 = new Watermelon();
		Fruits watermelon5 = new Watermelon();
		Fruits watermelon6 = new Watermelon();
		Fruits watermelon7 = new Watermelon();
		Fruits watermelon8 = new Watermelon();
		
		List<Fruits> fruits = new ArrayList<>();
		fruits.add(apple1);
		fruits.add(apple2);
		fruits.add(apple3);
		fruits.add(apple4);
		fruits.add(apple5);
		
		fruits.add(orange1);
		fruits.add(orange2);
		fruits.add(orange3);
		fruits.add(orange4);
		
		fruits.add(watermelon1);
		fruits.add(watermelon2);
		fruits.add(watermelon3);
		fruits.add(watermelon4);
		fruits.add(watermelon5);
		fruits.add(watermelon6);
		fruits.add(watermelon7);
		fruits.add(watermelon8);
		
		// Expected
		// 5 apple (3x 0.2 + 2 free)
		// 4 orange (4x 0.5)
		// 8 watermelon (6x 0.8 + 2 free)
		Float expectedTotalPrice = 7.4f;
		
		// Execute
		Float resultTotalPrice = discountService.computeDiscount(fruits);
		
		// Assert
		Assert.assertEquals(expectedTotalPrice, resultTotalPrice, 0.0f);
		
	}
	
	@Test
	public void testComputeDiscount_supermarketcase() {
		
		// Data
		Fruits apple1 = new Apple();
		Fruits apple2 = new Apple();
		Fruits apple3 = new Apple();
		Fruits apple4 = new Apple();
		
		Fruits orange1 = new Orange();
		Fruits orange2 = new Orange();
		Fruits orange3 = new Orange();
		
		Fruits watermelon1 = new Watermelon();
		Fruits watermelon2 = new Watermelon();
		Fruits watermelon3 = new Watermelon();
		Fruits watermelon4 = new Watermelon();
		Fruits watermelon5 = new Watermelon();
		
		List<Fruits> fruits = new ArrayList<>();
		fruits.add(apple1);
		fruits.add(apple2);
		fruits.add(apple3);
		fruits.add(apple4);
		
		fruits.add(orange1);
		fruits.add(orange2);
		fruits.add(orange3);
		
		fruits.add(watermelon1);
		fruits.add(watermelon2);
		fruits.add(watermelon3);
		fruits.add(watermelon4);
		fruits.add(watermelon5);
		
		// Expected
		// 4 apple (2x 0.2 + 2 free)
		// 3 orange (3x 0.5)
		// 5 watermelon (4x 0.8 + 1 free)
		Float expectedTotalPrice = 5.1f;
		
		// Execute
		Float resultTotalPrice = discountService.computeDiscount(fruits);
		
		// Assert
		Assert.assertEquals(expectedTotalPrice, resultTotalPrice, 0.0f);
		
	}
	
}
